<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Helper factory: Primero recibe el modelo y la cantidad de registros que deseamos crear
        factory('App\User', 10)->create();
        factory('App\Post', 30)->create();
        factory('App\Subscription', 2)->create();

        // $this->call(UsersTableSeeder::class);
    }
}
