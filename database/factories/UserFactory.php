<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//Este archivo trae definifo por defecto el factory para la tabla users
//$faker->text($maxNbChars = 50)

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt(1), //Seteamos todas las claves con el valor 1
        //Hash::make(1)
        'remember_token' => str_random(10),
    ];
});

//php artisan make:factory PostFactory
//Factory de posts
$factory->define(App\Post::class, function (Faker $faker) {
    return [
    	'title' => $faker->text($maxNbChars = 50),
    	'body' => $faker->text(),
    	'user_id' => rand(1, 10)
    ];
});

//Factory de subscriptions
$factory->define(App\Subscription::class, function (Faker $faker) {
    return [
    	'user_id' => rand(1, 10),
    	'post_id' => 1
    ];
});