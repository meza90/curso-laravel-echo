<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = ['user_id', 'post_id', 'comment'];

	protected $appends = ['created_at_ago'];

    //Solamente pertenece a un usuario
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    //Pertenece solamente a un post
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function getCreatedAtAgoAttribute()
    {
    	return $this->created_at->diffForHumans();
    }
}
