<?php

namespace App;

use App\Notifications\SubscribersCommentNotification;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $appends = ['created_at_format'];

    //Solamente pertenece a un usuario
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    //Puede tener muchos comentarios
    public function comments()
    {
    	return $this->hasMany(Comment::class)
                ->orderByDesc('created_at');
    }

    //Pueden estar suscritos muchos usuarios
    public function subscriptions()
    {
    	return $this->hasMany(Subscription::class);
    }

    public function getCreatedAtFormatAttribute()
    {
        return $this->created_at->format('d/m/Y H:i');
    }

    public function subscribers()
    {
        //Vamos a obtener todos los user_id que están suscritos a un post en particular
        //Si el usuario que comentó está SUSCRITO al post, vamos a IGNORAR su USER_ID, asi evitamos notificarle a él mismo, para eso simplemente vamos a usar un WHERE antes del método PLUCK.
        //En el where el USER_ID va a ser DISTINTO del id del usuario que envío el comentario
        $users = $this->subscriptions
                    ->where('user_id', '!=', auth()->id())
                    ->pluck('user_id');

        //Solamente vamos a ejecutar el siguiente codigo
        //si hay al menos UN USUARIO suscrito al post que se está comentando

        if(count($users) > 0) {
            $subs = User::whereIn('id', $users)->get();

            foreach ($subs as $user) {
                //vamos a notificar a cada usuario, cada vez que se haga un comentario en un post al cual está suscrito
                $user->notify(
                    new SubscribersCommentNotification
                    (
                        auth()->user(), $this
                    )
                );
            }
        }

    }
}
