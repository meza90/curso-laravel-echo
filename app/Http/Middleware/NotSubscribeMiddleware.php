<?php

namespace App\Http\Middleware;

use Closure;

class NotSubscribeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //vamos a obtener el valor del campo user_id del post
        if ($request->route('post')->user_id === auth()->id()) {
            session()->flash('error', 'No puedes suscribirte a tu post');
            return back();
            # code...
        }
        return $next($request);
    }
}
