<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Notifications\PostCommentNotification;
use App\Post;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user')->orderByDesc('created_at')->get();

        return view('posts.index', compact('posts'));
    }

    /**
     * Guardar nuevos comentarios en la base de datos
     * @param Post $post
     * @return Response
     */
    public function saveComment(Post $post)
    {
        //El codigo para ejecutar las notificaciones está en la clase CommentObserver metodo created
        return Comment::create([
            'user_id' => auth()->id(),
            'post_id' => $post->id,
            'comment' => request()->comment
        ])->load('user');
    }

    /**
     * Obtener los comentarios de un post
     * @param Post $post
     */
    public function getComments(Post $post)
    {
        return $post->load('comments.user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Guardar nueva suscripcion
     * @param Post $post
     * @return Illuminate\Http\Response
     */
    public function subscribe(Post $post)
    {
        Subscription::create([
            'user_id' => auth()->id(),
            'post_id' => $post->id
        ]);

        return back();
    }
}
