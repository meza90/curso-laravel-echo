<?php

namespace App\Notifications;

use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PostCommentNotification extends Notification implements ShouldQueue
{
    public $user;
    public $post;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Post $post)
    {
        //
        $this->user = $user;
        $this->post = $post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Notification broadcast
     * @param type $notifiable
     * @return Illuminate\Http\Resources\Json\response
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'data' => [
                'post_id' => $this->post->id,
                'post_title' => $this->post->title,
                'user_id' => $this->user->id,
                'user_name' => $this->user->name,
            ],
            'created_at_format' => Carbon::now()
                ->format('d/m/Y H:i'),
            'read_at' => false
        ]);
    }

    /**
     * Get the array representation of the notification.
     * Get the array representation of the notification.
     * En este metodo definimos a través de un array la información
        que le vamos a mostrar al usuario y que se va a insertar en formato JSON en el campo data de la tabla notifications .
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'post_id' => $this->post->id,
            'post_title' => $this->post->title,
            'user_id' => $this->user->id,
            'user_name' => $this->user->name
        ];
    }
}
