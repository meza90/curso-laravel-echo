<?php

namespace App\Observers;

use App\Comment;
use App\Events\PostPresenceEvent;
use App\Notifications\PostCommentNotification;

class CommentObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\User  $user
     * @return void
     */
    #Cuando se guarde un NUEVO comentario en la DB, se va a disparar el evento CREATED y se va a ejecutar el codigo que hayamos definido dentro de la función.
    public function created(Comment $comment)
    {
        $post = $comment->post;

        //Le vamos a notificar al DUEÑO del post que se realizo un nuevo comentario
        //Solamente vamos a enviar la notificación al dueño del POST cuando el valor del campo USER_ID del post sea DISTINTO al ID del usuario que está logueado en la aplicación
        if($post->user_id !== auth()->id()) {
            $post->user->notify(
                new PostCommentNotification(
                    auth()->user(), $post
                )
            );
        }

        //Vamos a notificar a los usuarios que estan SUSCRITOS al post que se realizo un nuevo comentario
        $post->subscribers();

        broadcast(new PostPresenceEvent($post->id, $comment->comment))
                ->toOthers();
    }
}