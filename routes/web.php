<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('posts', 'PostController@index');
Route::get('posts/{post}', 'PostController@show');

Route::prefix('api')->group(function(){
	Route::get('posts/{post}/comments', 'PostController@getComments');
	Route::post('posts/{post}/comment', 'PostController@saveComment')
				->name('posts.comments');
	Route::get('notifications', 'HomeController@getNotifications');
});

Route::post('posts/{post}/subscribe', 'PostController@subscribe')
		->name('posts.susbscriptions')
		->middleware('not.subscribe');