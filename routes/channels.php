<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

//Este canal solamente podemos escucharlo a traves de un canal privado, asi que es olbigacion estar logueados en la aplicacion,

//$user: Parametro que usa Laravel para devolver/obtener al usuario que está logueado.
//$id: Este parametro lo pasamos nosotros y debe ser igual al $id del usuario que está logueado.

//Para que el usuario tenga autorización a este canal, el id del usuario que está logueado, debe ser igual al id que pasamos nosotros.
Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('post-presence-{id}', function ($user, $id) {
    return ['id' => $user->id, 'name' => $user->name];
});