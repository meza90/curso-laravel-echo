@if (auth()->user()->isSubscribed($post->id))
    <button type="button" class="btn btn-default btn-lg disabled">
        Estás suscrito
    </button>
@else
    <form method="POST"
        action="{{ route('posts.susbscriptions', [$post->id]) }}">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-default btn-lg">
            Suscribirse
        </button>
    </form>
@endif