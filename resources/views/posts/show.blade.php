@extends('layouts.app')

@section('content')

<div class="container">
    @if (session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6">
            <h3>{{ $post->title }}</h3>
            <p class="lead">
                <i class="fa fa-user"></i>
                {{ $post->user->name }}
            </p>

            <hr>

            <p><i class="fa fa-calendar"></i>
            {{ $post->created_at_format }}
            </p>

            <hr>

            <p>
                {{ $post->body }}
            </p>
            @auth
                @if($post->user_id !== auth()->id())
                    @include('posts.subscribed')
                @endif
            @else
                <a href="{{ url('login') }}" class="btn btn-default btn-lg">
                    Inicia sesión para suscribirte
                </a>
            @endauth
        </div>
        <div class="col-lg-6">
            @auth
                <post-component :post_id="{{ $post->id }}"></post-component>
            @else
                <div class="alert alert-info">
                    <strong>Inicia sesión para comentar</strong>
                </div>
            @endauth
        </div>
    </div>
</div>

@endsection