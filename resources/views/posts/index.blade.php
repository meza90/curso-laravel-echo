@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
    	<div class="page-header">
    	  <h1>Últimos posts</h1>
    	</div>

        <div class="col-md-8">
            <div class="row">
                @foreach ($posts as $post)
                <div class="col-md-12 post">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>
                                <strong><a href="{{ url('posts', [$post->id]) }}" class="post-title">
                                    {{ $post->title }}
                                </a></strong></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 post-header-line">
                            <span class="fa fa-user"></span> Por <a href="#">
                            {{ $post->user->name }}</a> | <span class="fa fa-calendar">
                            </span>
                            {{ $post->created_at_format }}
                        </div>
                    </div>
                    <div class="row post-content">
                        <div class="col-md-9">
                            <p>
                                {{ $post->body }}
                            </p>
                            <p>
                                <a class="btn btn-default" href="{{ url('posts', [$post->id]) }}">Ver más</a></p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <hr>
</div>

@endsection